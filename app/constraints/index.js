var SERVER_PORT = 3000;
var SERVICE_API_URL = 'travellogix.api.test.conceptsol.com';
var CURRENT_TOKEN = '';

module.exports.port = SERVER_PORT;
module.exports.service_api_url = SERVICE_API_URL;
module.exports.current_token = CURRENT_TOKEN;
