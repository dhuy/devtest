var constraints = require('../constraints/index');
var http = require('http');

function registerRoutes(app) {
	/*
	* Rota padrão
	*/
	app.get('/', function (req, res) {
	  res.send('Web API online.');
	});

	/*
	* Rota que consulta o token de autenticação
	*/
	app.post('/api/Token', function (req, res) {
		var options = {
			host: constraints.service_api_url,
			port: 80,
			path: '/Token',
			method: 'POST',
			headers: {}
		};

		var request = http.request(options, function(response) {
			var body = '';

			response.setEncoding('utf8');

			response.on('readable', function () {
        var chunk = this.read() || '';

        body += chunk;
    	});

			response.on('end', function () {
				var json = JSON.parse(body);

				/*
				* Guardando o token em memória
				*/
				constraints.current_token = json.token_type + ' ' + json.access_token;

	      res.status(200).send(json);

			});

			response.on('error', function(e) {
				res.status(500).send({ "error": e.message });
			});
		});

		request.write(req.body);
		request.end();
	});

	/*
	* Rota que consulta os dados
	*/
	app.post('/api/Ticket/Search', function (req, res) {
		var options = {
			host: constraints.service_api_url,
			port: 80,
			path: '/api/Ticket/Search',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': constraints.current_token
			}
		};

		var request = http.request(options, function(response) {
			var body = '';

			response.setEncoding('utf8');

			response.on('readable', function () {
        var chunk = this.read() || '';

        body += chunk;
    	});

    	response.on('end', function () {
	      res.status(200).send(JSON.parse(body));
	    });

	    response.on('error', function(e) {
	    	res.status(500).send({ "error": e.message });
	    });
		});

		request.write(JSON.stringify(req.body));
		request.end();
	});
};

module.exports.register = registerRoutes;