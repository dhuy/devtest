var constraints = require('./app/constraints/index');
var routes = require('./app/routes/index');
var bodyParser = require('body-parser');

var express = require('express');
var app = express();

app.use(bodyParser.text());
app.use(bodyParser.json());

routes.register(app);

app.listen(constraints.port, function () {
  console.log('Web API funcionando na porta ' + constraints.port);
});